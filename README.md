# Requirements

SCSS to CSS Template requires compass on the local environment to run.

# Quick Overview
- Download or clone this repository to your development environment.
- Install Compass on your computer [Read Compass documentation here http://compass-style.org/help].
- Open command line and change directory to the project folder [cd scss-template].
- Run compass [compass watch].

```sh
cd scss-template
compass watch
```

# Demo

http://www.rascojet.com/github/scss-template

## License

[MIT](http://opensource.org/licenses/MIT)